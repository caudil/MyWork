<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JadwalPelajaranController extends Controller
{
    function index(Request $request){
        $jadwalpelajaran = \App\Hari::with('JadwalPelajaran','JadwalPelajaran.MataPelajaran')->get();
        return \Response::json($jadwalpelajaran,200);
    }
}