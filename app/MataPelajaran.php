<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table = "matapelajaran";

    public function JadwalPelajaran()
    {
        return $this->hasMany('App\JadwalPelajaran','idmatapelajaran');
    }
}
