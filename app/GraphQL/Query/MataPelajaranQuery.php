<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\MataPelajaran;

class MataPelajaranQuery extends Query
{
    protected $attributes = [
        'name' => 'MataPelajaranQuery',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQl::type('MataPelajaranType'));
    }

    public function args()
    {
        return [
            'id' => [
                'type' => Type::Int(),
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if(isset($args['id'])){
            return MataPelajaran::where('id',$args['id'])->get();
        }else{
            return MataPelajaran::all();
        }
    }
}
