<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class UsersType extends BaseType
{
    protected $attributes = [
        'name' => 'Type User',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::Int()),
            ],
            'nama' => [
                'type' => Type::nonNull(Type::String()),
            ],
            'email' => [
                'type' => Type::nonNull(Type::String()),
            ],
            'bio' => [
                'type' => Type::String(),
            ],
        ];
    }
}
