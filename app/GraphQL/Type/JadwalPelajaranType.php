<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class JadwalPelajaranType extends BaseType
{
    protected $attributes = [
        'name' => 'JadwalPelajaranType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::Int()),
            ],
            'jampelajaran' => [
                'type' => Type::String(),
            ],
            'matapelajaran' => [
                'args' => [
                    'id' => [
                        'type' => Type::Int(),
                    ],
                    'mnmatapelajaran' => [
                        'type' => Type::String(),
                    ],
                ],
                'type' => GraphQL::type('MataPelajaranType'),
            ],
        ];
    }
}
