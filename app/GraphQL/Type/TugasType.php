<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class TugasType extends BaseType
{
    protected $attributes = [
        'name' => 'TugasType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::Int()),
            ],
            'judul' => [
                'type' => Type::String(),
            ],
            'keterangan' => [
                'type' => Type::String(),
            ],
            'file' => [
                'type' => Type::String(),
            ],
            'tglpengumpulan' => [
                'type' => Type::String(),
            ],
        ];
    }
}
