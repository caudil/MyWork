<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class MataPelajaranType extends BaseType
{
    protected $attributes = [
        'name' => 'MataPelajaranType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::Int()),
            ],
            'nmmatapelajaran' => [
                'type' => Type::String(),
            ],
            'nmguru' => [
                'type' => Type::String(),
            ],
            'keterangan' => [
                'type' => Type::String(),
            ],
            'jadwalpelajaran' => [
                'args' => [
                    'id' => [
                        'type' => Type::Int(),
                    ],
                    'jampelajaran' => [
                        'type' => Type::String(),
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('JadwalPelajaranType')),
            ],
        ];
    }
}
