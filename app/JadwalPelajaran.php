<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalPelajaran extends Model
{
    protected $table = "jadwalpelajaran";

    public function MataPelajaran()
    {
        return $this->belongsTo('App\MataPelajaran','idmatapelajaran');
    }
    
    public function Hari()
    {
        return $this->belongsTo('App\Hari','idhari');
    }
}
