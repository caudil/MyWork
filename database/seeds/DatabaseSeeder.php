<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(user::class);
        $this->call(matapelajaran::class);
        $this->call(hari::class);
        $this->call(jadwalpelajaran::class);
    }
}
