<?php

use Illuminate\Database\Seeder;

class jadwalpelajaran extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Senin
        DB::table('jadwalpelajaran')->insert([
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 1,
            ],
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 2,
            ],
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 3,
            ],
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 4,
            ],
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 5,
            ],
            [
                'idmatapelajaran' => 1,
                'idhari' => 1,
                'jampelajaran' => 6,
            ],
            [
                'idmatapelajaran' => 2,
                'idhari' => 1,
                'jampelajaran' => 7,
            ],
            [
                'idmatapelajaran' => 2,
                'idhari' => 1,
                'jampelajaran' => 8,
            ],
            [
                'idmatapelajaran' => 2,
                'idhari' => 1,
                'jampelajaran' => 9,
            ],
            [
                'idmatapelajaran' => 2,
                'idhari' => 1,
                'jampelajaran' => 10,
            ],
        ]);

        // Selasa
        DB::table('jadwalpelajaran')->insert([
            [
                'idmatapelajaran' => 3,
                'idhari' => 2,
                'jampelajaran' => 1,
            ],
            [
                'idmatapelajaran' => 3,
                'idhari' => 2,
                'jampelajaran' => 2,
            ],
            [
                'idmatapelajaran' => 4,
                'idhari' => 2,
                'jampelajaran' => 3,
            ],
            [
                'idmatapelajaran' => 4,
                'idhari' => 2,
                'jampelajaran' => 4,
            ],
            [
                'idmatapelajaran' => 5,
                'idhari' => 2,
                'jampelajaran' => 5,
            ],
            [
                'idmatapelajaran' => 5,
                'idhari' => 2,
                'jampelajaran' => 6,
            ],
            [
                'idmatapelajaran' => 6,
                'idhari' => 2,
                'jampelajaran' => 7,
            ],
            [
                'idmatapelajaran' => 6,
                'idhari' => 2,
                'jampelajaran' => 8,
            ],
            [
                'idmatapelajaran' => 6,
                'idhari' => 2,
                'jampelajaran' => 9,
            ],
            [
                'idmatapelajaran' => 6,
                'idhari' => 2,
                'jampelajaran' => 10,
            ],
        ]);

        // Rabu
        DB::table('jadwalpelajaran')->insert([
            [
                'idmatapelajaran' => 7,
                'idhari' => 3,
                'jampelajaran' => 1,
            ],
            [
                'idmatapelajaran' => 7,
                'idhari' => 3,
                'jampelajaran' => 2,
            ],
            [
                'idmatapelajaran' => 8,
                'idhari' => 3,
                'jampelajaran' => 3,
            ],
            [
                'idmatapelajaran' => 8,
                'idhari' => 3,
                'jampelajaran' => 4,
            ],
            [
                'idmatapelajaran' => 9,
                'idhari' => 3,
                'jampelajaran' => 5,
            ],
            [
                'idmatapelajaran' => 9,
                'idhari' => 3,
                'jampelajaran' => 6,
            ],
            [
                'idmatapelajaran' => 10,
                'idhari' => 3,
                'jampelajaran' => 7,
            ],
            [
                'idmatapelajaran' => 10,
                'idhari' => 3,
                'jampelajaran' => 8,
            ],
            [
                'idmatapelajaran' => 11,
                'idhari' => 3,
                'jampelajaran' => 9,
            ],
            [
                'idmatapelajaran' => 11,
                'idhari' => 3,
                'jampelajaran' => 10,
            ],
        ]);

        // Kamis
        DB::table('jadwalpelajaran')->insert([
            [
                'idmatapelajaran' => 12,
                'idhari' => 4,
                'jampelajaran' => 1,
            ],
            [
                'idmatapelajaran' => 12,
                'idhari' => 4,
                'jampelajaran' => 2,
            ],
            [
                'idmatapelajaran' => 12,
                'idhari' => 4,
                'jampelajaran' => 3,
            ],
            [
                'idmatapelajaran' => 13,
                'idhari' => 4,
                'jampelajaran' => 4,
            ],
            [
                'idmatapelajaran' => 13,
                'idhari' => 4,
                'jampelajaran' => 5,
            ],
            [
                'idmatapelajaran' => 13,
                'idhari' => 4,
                'jampelajaran' => 6,
            ],
            [
                'idmatapelajaran' => 14,
                'idhari' => 4,
                'jampelajaran' => 7,
            ],
            [
                'idmatapelajaran' => 14,
                'idhari' => 4,
                'jampelajaran' => 8,
            ],
            [
                'idmatapelajaran' => 14,
                'idhari' => 4,
                'jampelajaran' => 9,
            ],
            [
                'idmatapelajaran' => 14,
                'idhari' => 4,
                'jampelajaran' => 10,
            ],
        ]);

        // Jumat
        DB::table('jadwalpelajaran')->insert([
            [
                'idmatapelajaran' => 11,
                'idhari' => 5,
                'jampelajaran' => 1,
            ],
            [
                'idmatapelajaran' => 11,
                'idhari' => 5,
                'jampelajaran' => 2,
            ],
            [
                'idmatapelajaran' => 15,
                'idhari' => 5,
                'jampelajaran' => 3,
            ],
            [
                'idmatapelajaran' => 15,
                'idhari' => 5,
                'jampelajaran' => 4,
            ],
            [
                'idmatapelajaran' => 16,
                'idhari' => 5,
                'jampelajaran' => 5,
            ],
            [
                'idmatapelajaran' => 16,
                'idhari' => 5,
                'jampelajaran' => 6,
            ],
            [
                'idmatapelajaran' => 17,
                'idhari' => 5,
                'jampelajaran' => 7,
            ],
            [
                'idmatapelajaran' => 17,
                'idhari' => 5,
                'jampelajaran' => 8,
            ],
            [
                'idmatapelajaran' => 17,
                'idhari' => 5,
                'jampelajaran' => 9,
            ],
            [
                'idmatapelajaran' => 17,
                'idhari' => 5,
                'jampelajaran' => 10,
            ],
        ]);
    }
}
